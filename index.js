
import fetch from 'node-fetch';
const delay = (delayInms) => {
    return new Promise(resolve => setTimeout(resolve, delayInms));
}

const generateOrders = () => {
    const orderCount = 100;
    const orderStatusCount = 4;
    const orderStart = 1000;
    const orders = [];
    for(let i=0; i<orderCount/orderStatusCount; i++) {
        for (let j=1; j<=orderStatusCount; j++) {
            orders.push({
                orderNumber: (orderStart + i),
                orderStatus: j
            })
        }
    }
    return orders;
}

const processOrder = async (url, options) => {
    const response = await fetch(url, options);
    if (!response.ok) {
      const errorMessage = await response.text();
      throw new Error(`Request '${url}' failed with ${response.status}: ${response.statusText}: ${errorMessage}`);
    }
    return;
}

const processAllOrders = async () => {

    const orders = generateOrders()

    for (let order of orders) {
        try {
            console.log(`pushing order ${order.orderNumber}`)
            await processOrder("https://shark-app-uiqkf.ondigitalocean.app/api/order/process", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(order)
            })
        }catch(err) {
            console.log(`error pushing order ${order.orderNumber}`)
        }
        await delay(200);
    }
    console.log('Script execution complete')
}

processAllOrders();
